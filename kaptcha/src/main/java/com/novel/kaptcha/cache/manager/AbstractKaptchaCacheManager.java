package com.novel.kaptcha.cache.manager;

import com.novel.kaptcha.cache.KaptchaCache;
import com.novel.kaptcha.exception.KaptchaCacheException;
import org.springframework.util.StringUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 验证码缓存管理器实现类
 *
 * @author novel
 * @date 2019/12/2
 */
public abstract class AbstractKaptchaCacheManager implements KaptchaCacheManager {
    private final ConcurrentMap<String, KaptchaCache> caches = new ConcurrentHashMap<>();

    @Override
    public KaptchaCache getCache(String key) throws KaptchaCacheException {
        try {
            if (StringUtils.hasText(key)) {
                KaptchaCache kaptchaCache = this.caches.get(key);
                if (kaptchaCache == null) {
                    kaptchaCache = this.createCache(key);
                    KaptchaCache existing = this.caches.putIfAbsent(key, kaptchaCache);
                    if (existing != null) {
                        kaptchaCache = existing;
                    }
                }
                return kaptchaCache;
            } else {
                throw new KaptchaCacheException();
            }
        } catch (Exception e) {
            throw new KaptchaCacheException("Cache key cannot be null or empty.");
        }
    }

    /**
     * 创建一个验证码缓存对象
     *
     * @param key 缓存对象的key
     * @return 验证码缓存对象
     * @throws KaptchaCacheException 验证码缓存异常
     */
    abstract KaptchaCache createCache(String key) throws KaptchaCacheException;

    /**
     * 销毁一个验证码缓存对象
     *
     * @throws KaptchaCacheException 验证码缓存异常
     */
    public void destroy() throws KaptchaCacheException {
        try {
            if (!this.caches.isEmpty()) {
                for (KaptchaCache kaptchaCache : this.caches.values()) {
                    if (kaptchaCache != null) {
                        kaptchaCache.destroy();
                    }
                }
                this.caches.clear();
            }
        } catch (Exception e) {
            throw new KaptchaCacheException();
        }
    }
}
