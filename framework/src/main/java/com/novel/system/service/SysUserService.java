package com.novel.system.service;

import com.novel.system.domain.SysUser;

import java.util.List;

/**
 * 用户服务层
 *
 * @author novel
 * @date 2019/12/20
 */
public interface SysUserService {

    /**
     * 根据用户名查询用户
     *
     * @param username 用户名
     * @return 用户
     */
    SysUser findUserByUserName(String username);

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 用户信息
     */
    SysUser login(String username, String password);

    /**
     * 根据条件分页查询用户对象
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    List<SysUser> selectUserList(SysUser user);

    /**
     * 保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean updateUser(SysUser user);

    /**
     * 更新用户登录相关信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean updateUserLoginInfo(SysUser user);

    /**
     * 保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean insertUser(SysUser user);

    /**
     * 批量添加用户
     *
     * @param userList 用户集合
     * @return 结果
     */
    boolean insertUser(List<SysUser> userList);

    /**
     * 删除用户信息
     *
     * @param id 用户ID
     * @return 结果
     */
    boolean deleteUserById(Long id);

    /**
     * 批量删除用户信息
     *
     * @param ids 用户ID
     * @return 结果
     */
    boolean deleteUserByIds(Long[] ids);

    /**
     * 通过用户ID查询用户
     *
     * @param id 用户ID
     * @return 用户对象信息
     */
    SysUser selectUserById(Long id);

    /**
     * 通过用户ID查询用户
     *
     * @param id 用户ID
     * @return 用户对象信息
     */
    SysUser selectUserByIdAndDept(Long id);

    /**
     * 修改用户详细信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean updateUserInfo(SysUser user);

    /**
     * 修改用户密码信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean modifyPassword(SysUser user, String oldPassWord, String newPassword);

    /**
     * 重置密码
     *
     * @param userId 用户id
     * @return 结果
     */
    boolean resetUserPwd(Long userId, String newPassword);


    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    String checkUserNameUnique(SysUser user);


    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    String checkPhoneUnique(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    String checkEmailUnique(SysUser user);
}
