package com.novel.framework.utils;

import com.alibaba.fastjson.JSONObject;
import com.novel.common.utils.Ip2RegionUtil;
import com.novel.common.utils.StringUtils;
import com.novel.common.utils.model.IpInfo;
import com.novel.framework.config.ProjectConfig;
import com.novel.framework.utils.http.HttpUtils;
import com.novel.framework.utils.servlet.IpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 获取ip真实地址类
 *
 * @author novel
 * @date 2019/12/19
 */
public class AddressUtils {
    private static final Logger log = LoggerFactory.getLogger(AddressUtils.class);

    public static final String IP_URL = "http://ip.taobao.com/service/getIpInfo.php";

    public static final String FAIL_ADDRESS = "XX XX";

    public static String getRealAddress(String ip) {
        String address = FAIL_ADDRESS;
        if (IpUtils.internalIp(ip)) {
            return "内网IP";
        }
        if (ProjectConfig.getAddressEnabled()) {
            address = getRealAddressByLocal(ip);
            if (StringUtils.equals(FAIL_ADDRESS, address)) {
                //表示本地方式获取地址失败
                address = getRealAddressByIP(ip);
                if (StringUtils.equals(FAIL_ADDRESS, address)) {
                    //表示通过网络获取地址也失败了,再获取一次，因为taobao地址经常502
                    address = getRealAddressByIP(ip);
                }
            }
        }

        return address;
    }


    /**
     * 根据请求taobao ip接口获取ip真实地址
     *
     * @param ip ip地址
     * @return 真实地址
     */
    public static String getRealAddressByIP(String ip) {
        String address = FAIL_ADDRESS;

        String rspStr = HttpUtils.sendPost(IP_URL, "ip=" + ip);
        if (StringUtils.isEmpty(rspStr)) {
            log.error("获取地理位置异常 {}", ip);
            return address;
        }
        JSONObject obj = JSONObject.parseObject(rspStr);
        JSONObject data = obj.getObject("data", JSONObject.class);
        String region = data.getString("region");
        String city = data.getString("city");
        address = region + " " + city;
        return address;
    }

    /**
     * 根据本地ip库获取ip真实地址
     *
     * @param ip ip地址
     * @return 真实地址
     */
    public static String getRealAddressByLocal(String ip) {
        String address = FAIL_ADDRESS;

        try {
            IpInfo ipInfo = Ip2RegionUtil.find(ip);
            if (ipInfo != null && !"0".equalsIgnoreCase(ipInfo.getProvince()) && !"0".equalsIgnoreCase(ipInfo.getCity())) {
                address = ipInfo.getProvince() + " " + ipInfo.getCity();
            }
        } catch (Exception e) {
            log.error("根据IP获取所在位置----------错误消息：" + e.getMessage());
        }
        return address;
    }

}
