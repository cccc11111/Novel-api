package com.novel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 启动程序
 *
 * @author novel
 * @date 2019/12/5
 */
@SpringBootApplication
@Slf4j
public class NovelApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(NovelApplication.class, args);
        log.info("启动系统");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        log.info("启动系统");
        return application.sources(NovelApplication.class);
    }
}
